﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

	public PlayerControl player;
	public GameManager gM;
	public float approachingSpeedFactor = 2;
	public float leavingSpeedFactor = 4;
	public float distanceFromPlayer = 5;

	AudioSource aS;
	public AudioClip slow, med, fast, faster, attack, punch;

	Animator anim;

	Rigidbody rB;

	bool isAttacking = false;

	void Start () {
		aS = GetComponent<AudioSource> ();
		anim = GetComponent<Animator>();
		rB = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		if (!gM.isGameOver) {
			
			if (gM.phaseNum == 1) { //Warm Up Phase
				anim.SetTrigger("isCrawling");
				rB.velocity = player.speed;
			}
			else if (gM.phaseNum == 2) { //High Intensity Phase
				anim.SetBool("isCrawling", false);
				anim.SetBool("isFastRunning", true);
				//anim.SetFloat("Speed",1f);	
				if (player.exSpeed < gM.targetSpeed - gM.targetSpeedRange || player.exSpeed > gM.targetSpeed + gM.targetSpeedRange) {
					if(!isAttacking){
						anim.SetFloat("Speed",1.4f);
						aS.clip = faster;
					}
					rB.velocity = player.speed + player.transform.right * approachingSpeedFactor;
				} else {
					aS.clip = fast;
					anim.SetFloat("Speed",1f);
					transform.position = Vector3.Lerp(transform.position, player.transform.position - new Vector3(0,0.4f,0) - player.transform.right * distanceFromPlayer, Time.deltaTime);
					rB.velocity = player.speed;
				}
			} else if (gM.phaseNum == 3) { //Low Intensity Phase
				anim.SetBool("isFastRunning", false);
				anim.SetFloat("Speed",1f);
				if (player.exSpeed < gM.targetSpeed - gM.targetSpeedRange || player.exSpeed > gM.targetSpeed + gM.targetSpeedRange) {
					if(!isAttacking){
						anim.SetBool("isFastRunning", true); // instead of this, maybe speed change?
						aS.clip = fast;
					}
					rB.velocity = player.speed + player.transform.right * approachingSpeedFactor;
				} else {
					anim.SetBool("isFastRunning", false);
					aS.clip = med;
					transform.position = Vector3.Lerp(transform.position, player.transform.position - new Vector3(0,0.4f,0) - player.transform.right * distanceFromPlayer, Time.deltaTime); //Vector3(0,0.4f,0) is needed to make YETI touch on the ground
					rB.velocity = player.speed;
				}
			}

			else if (gM.phaseNum == 4) { //Cooling Down Phase
				anim.SetFloat("Speed",1f);
				anim.SetBool("isFastRunning", false); //in any case to return to running state
				anim.SetTrigger("isCrawling");
				aS.clip = slow;
				//rB.velocity = player.speed - player.transform.right * leavingSpeedFactor;
				rB.velocity = Vector3.Lerp(rB.velocity, Vector3.zero, Time.deltaTime*0.2f);
				rB.isKinematic = true;
			}

			float distance = Vector3.Distance(player.transform.position,transform.position);

			if (distance < 1f) {
				gM.isGameOver = true;
				aS.clip = punch;
			}

			else if (distance < 3.5f) {
				isAttacking = true;
				aS.clip = attack;
				aS.loop = false;
				anim.SetTrigger("isAttacking");
			}

			else{
				isAttacking = false;
				anim.SetBool("isAttacking", false);
			}

			if (!aS.isPlaying && (gM.phaseNum == 2 || gM.phaseNum == 3)) {
				aS.Play();
			}
		}
		else{
			rB.velocity = Vector3.Lerp(rB.velocity, Vector3.zero, Time.deltaTime*0.2f);
			rB.isKinematic = true;
		}

		print(gM.isGameOver);
	}
}
