﻿using UnityEngine;
using System.Collections;

public class GaugeBehavior : MonoBehaviour {

	public PlayerControl _pc;
	float speed;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		speed = _pc.exSpeed;
		float maxRotateAnlge = -1.2f * speed;
		Quaternion rotate = Quaternion.Euler (0, 0, 90+maxRotateAnlge);
		transform.rotation = Quaternion.Lerp (transform.rotation, rotate, Time.deltaTime * 2);


	}
}
