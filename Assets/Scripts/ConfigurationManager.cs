﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ConfigurationManager : MonoBehaviour {

	public InputField warmUpSpeedInput;
	public InputField hISpeedInput;
	public InputField lISpeedInput;
	public InputField coolDownSpeedInput;

	public InputField warmUpDurationInput;
	public InputField hIDurationInput;
	public InputField lIDurationInput;
	public InputField coolDownDurationInput;

	public Button speedConfirmButton;
	public Button durationConfirmButton;

	bool speedInputsCompleted;
	bool durationInputsCompleted;

	public GameObject confirmPanel;
	bool confirmed;
	string pressedConfirmButton = "";

	public GameObject speedConfirmed;
	public GameObject durationConfirmed;
	public GameObject inCompletionWarning;

	public GameObject playButton;

	[HideInInspector]
	public int warmUpSpeed;
	[HideInInspector]
	public int hISpeed;
	[HideInInspector]
	public int lISpeed;
	[HideInInspector]
	public int coolDownSpeed;

	[HideInInspector]
	public float warmUpDuration;
	[HideInInspector]
	public float hIDuration;
	[HideInInspector]
	public float lIDuration;
	[HideInInspector]
	public float coolDownDuration;

	// Use this for initialization
	void Start () {
		confirmPanel.SetActive (false);
		confirmed = false;
		PlayerPrefs.DeleteAll ();

		speedInputsCompleted = false;
		durationInputsCompleted = false;

		speedConfirmed.SetActive (false);
		durationConfirmed.SetActive (false);
		inCompletionWarning.SetActive (false);

		playButton.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if (warmUpSpeedInput.text != "") {
			warmUpSpeed = GetSpeedFromInputField (warmUpSpeedInput);
		}
		if (hISpeedInput.text != "") {
			hISpeed = GetSpeedFromInputField (hISpeedInput);
		}
		if (lISpeedInput.text != "") {
			lISpeed = GetSpeedFromInputField (lISpeedInput);
		}
		if (coolDownSpeedInput.text != "") {
			coolDownSpeed = GetSpeedFromInputField (coolDownSpeedInput);
		}

		if (warmUpDurationInput.text != "") {
			warmUpDuration = GetDurationFromInputField (warmUpDurationInput);
		}
		if (hIDurationInput.text != "") {
			hIDuration = GetDurationFromInputField (hIDurationInput);
		}
		if (lIDurationInput.text != "") {
			lIDuration = GetDurationFromInputField (lIDurationInput);
		}
		if (coolDownDurationInput.text != "") {
			coolDownDuration = GetDurationFromInputField (coolDownDurationInput);
		}

		CheckInputs ();
		ShowPlayButton ();

	}

	void CheckInputs()
	{
		if (warmUpSpeed != 0 && hISpeed !=0 && lISpeed != 0 && coolDownSpeed != 0) {
			speedInputsCompleted = true;

		} else{
			speedInputsCompleted = false;
		}

		if (warmUpDuration != 0.0 && hIDuration != 0.0 && lIDuration != 0.0 && coolDownDuration != 0.0) {
			durationInputsCompleted = true;
		}else{
			durationInputsCompleted = false;
		}
	}

	int GetSpeedFromInputField(InputField input)
	{
		int speed;
		speed = System.Convert.ToInt32 (input.text);
		return speed;
	}

	float GetDurationFromInputField(InputField input)
	{
		float duration;
		duration = System.Convert.ToSingle(input.text);
		return duration;
	}

	public void ConfirmSpeedInput()
	{
		pressedConfirmButton = "speed";
		if (confirmed && speedInputsCompleted) {
			PlayerPrefs.SetInt ("WarmUpSpeed", warmUpSpeed);
			PlayerPrefs.SetInt ("HISpeed", hISpeed);
			PlayerPrefs.SetInt ("LISpeed", lISpeed);
			PlayerPrefs.SetInt ("CoolDownSpeed", coolDownSpeed);

			speedConfirmed.SetActive (true);
			warmUpSpeedInput.DeactivateInputField ();
			hISpeedInput.DeactivateInputField ();
			lISpeedInput.DeactivateInputField ();
			coolDownSpeedInput.DeactivateInputField ();
			speedConfirmButton.gameObject.SetActive (false);
			confirmed = false;

		} else if (!confirmed && speedInputsCompleted) {
			confirmPanel.SetActive (true);

		} else if (!speedInputsCompleted) {
			inCompletionWarning.SetActive (true);
		}
	}

	public void ConfirmDurationInput()
	{
		pressedConfirmButton = "duration";
		if (confirmed && durationInputsCompleted) {
			PlayerPrefs.SetFloat ("WarmUpDuration", warmUpDuration);
			PlayerPrefs.SetFloat ("HIDuration", hIDuration);
			PlayerPrefs.SetFloat ("LIDuration", lIDuration);
			PlayerPrefs.SetFloat ("CoolDownDuration", coolDownDuration);

			durationConfirmed.SetActive (true);
			warmUpDurationInput.DeactivateInputField ();
			hIDurationInput.DeactivateInputField ();
			lIDurationInput.DeactivateInputField ();
			coolDownDurationInput.DeactivateInputField ();
			durationConfirmButton.gameObject.SetActive (false);
			confirmed = false;
		} else if (!confirmed && durationInputsCompleted) {
			confirmPanel.SetActive (true);
		} else if (!durationInputsCompleted) {
			inCompletionWarning.SetActive (true);
		}
	}

	public void Confirm()
	{
		confirmed = true;
		confirmPanel.SetActive (false);
		if(pressedConfirmButton == "speed")
			ConfirmSpeedInput();
		else
			ConfirmDurationInput();
	}

	public void CancelConfirmation()
	{
		confirmPanel.SetActive (false);
	}

	public void CloseWarning()
	{
		inCompletionWarning.SetActive (false);
	}

	public void LoadNextScene()
	{
		int thisScene = SceneManager.GetActiveScene().buildIndex;
		SceneManager.LoadScene (thisScene + 1);
	}

	public void ShowPlayButton()
	{
		if (!speedConfirmButton.IsActive() && !durationConfirmButton.IsActive()) {
			playButton.SetActive (true);
		}
	}
}
