﻿using UnityEngine;
using System.Collections;

public class BackgroundRecycle : MonoBehaviour {

	private GameObject mainCamera = null;

	private Vector3 initialPosition;

	public static float backgroundLength = 22.09f;

	public static int backgroundNumber= 4;

	// Use this for initialization
	void Start () {
		this.mainCamera = GameObject.FindGameObjectWithTag("Player");
		this.initialPosition = this.transform.position;

	
	}
	
	// Update is called once per frame
	void Update () {

		float		total_width = BackgroundRecycle.backgroundLength * BackgroundRecycle.backgroundNumber ;
		
		Vector3		camera_position = this.mainCamera.transform.position;
		
		float		dist = camera_position.z - this.initialPosition.z;

		int			n = Mathf.RoundToInt(dist/total_width);
		
		Vector3		position = this.initialPosition;
		
		position.z += n*total_width;
		
		this.transform.position = position;
	
	}
}
