﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FuelChargeManager : MonoBehaviour {

	float playerSpeed;
	float targetSpeed;

	float chargeGap = 1.0f;
	float nextTime = 0.0f;

	bool charging = false;

	public Slider fuelBar;
	float valueUnit;

	public Text takeOffLineText;

	bool decrease = false;

	// Use this for initialization
	void Start () {
		fuelBar.interactable = false;
		fuelBar.value = 0;
		valueUnit = 1 / (PlayerPrefs.GetFloat ("WarmUpDuration") + (PlayerPrefs.GetFloat ("HIDuration") + PlayerPrefs.GetFloat ("LIDuration"))* 3);
		print (valueUnit.ToString ());
	}

	// Update is called once per frame
	void Update () {
		playerSpeed = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerControl> ().exSpeed;
		targetSpeed = GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManager> ().targetSpeed;

		if (GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManager> ().phaseNum != 4 && GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManager> ().phaseNum != 0) {
			if (playerSpeed > (targetSpeed -20) && playerSpeed < (targetSpeed +20)) {
				charging = true;
			} else {
				charging = false;
			}

			if (charging) {
				if (Time.time > nextTime) {
					fuelBar.value += valueUnit;
					nextTime = Time.time + chargeGap;
				}
			}
		}

		if (GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManager> ().phaseNum == 4) {
			if (fuelBar.value > 0.5) {

				if (Time.time > nextTime) {
					decrease = true;
				}

				if (decrease) {
					fuelBar.value -= 1 / (PlayerPrefs.GetFloat ("CoolDownDuration") - 5.0f);
					nextTime = Time.time + 1.0f;
					decrease = false;
				}

			} else {

				if (Time.time > nextTime) {
					decrease = true;
				}

				if (decrease) {
					fuelBar.value -= 1 / (PlayerPrefs.GetFloat ("CoolDownDuration") + 5.0f);
					nextTime = Time.time + 1.0f;
					decrease = false;
				}
			}
		}

		if (fuelBar.value > 0.5) {
			takeOffLineText.color = Color.green;
		}
	}

}
