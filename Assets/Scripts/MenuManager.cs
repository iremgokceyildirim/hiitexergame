﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using System.IO;

public class MenuManager : MonoBehaviour {

	public RectTransform storyText;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Application.loadedLevelName == "Story" || Application.loadedLevelName == "How_to_Play"){
			float top = storyText.offsetMax.y;
			top += Time.deltaTime*15;
			storyText.offsetMax = new Vector2(storyText.offsetMax.x,top);
		}
	
	}

	public void onClickNewGame(){
		Application.LoadLevel("Game");
	}

	public void onClickStory(){
		Application.LoadLevel("Story");
	}

	public void onClickHowtoPlay(){
		Application.LoadLevel("How_to_Play");
	}

	public void onClickCredits(){
		Application.LoadLevel("Credits");
	}

	public void onClickQuit(){
		Application.Quit();
	}

	public void onClickBack(){
		Application.LoadLevel("Main_Menu");
	}

	public void onSubmitName(string name){
		JSONClass rootNode = new JSONClass();
		rootNode.Add("PlayerName",  new JSONData(name));
		var sr = File.CreateText("PlayerName.txt");
		sr.WriteLine (rootNode.ToString());
		sr.Close();
	}
}
