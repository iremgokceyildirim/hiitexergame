﻿using UnityEngine;
using System.Collections;

public class CaveRecycle : MonoBehaviour {

	public GameObject player;

	Vector3 initialPosition;

	public static float caveLength = 71.0f;

	public static int caveNumber= 3;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		this.initialPosition = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {

		float		total_width = CaveRecycle.caveLength * CaveRecycle.caveNumber ;

		Vector3		player_position = player.transform.position;

		float		dist = player_position.z - this.initialPosition.z;

		int			n = Mathf.RoundToInt(dist/total_width);

		Vector3		position = this.initialPosition;

		position.z += n*total_width;

		this.transform.position = position;

	
	}
}
