﻿using UnityEngine;
using System.Collections;

public class Mothership : MonoBehaviour {

	public Transform player;
	public float directionFactor = 1;

	// Use this for initialization
	void Start () {
	}


	// Update is called once per frame
	void Update () {
		transform.Translate(-Vector3.right * Time.deltaTime * directionFactor);
	}
}
