﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TutorialManager : MonoBehaviour {

	public GetMaxSpeed getMaxSpeed;


	public Button nextButton;

	float maxSpeed;

	// Use this for initialization
	void Start () {

		if (PlayerPrefs.HasKey ("PlayerMaxSpeed")) {
			PlayerPrefs.DeleteKey ("PlayerMaxSpeed");
		}

		nextButton.gameObject.SetActive (false);
	
	}
	
	// Update is called once per frame
	void Update () {

		if (!getMaxSpeed.testing & getMaxSpeed.maxSpeed > 0) {
			maxSpeed = getMaxSpeed.maxSpeed;
			PlayerPrefs.SetFloat ("PlayerMaxSpeed", maxSpeed);
			print ("Player's Max Speed =" + maxSpeed.ToString ());

			nextButton.gameObject.SetActive (true);
		}
	
	}

	public void LoadNextScene()
	{
		Scene scene = SceneManager.GetActiveScene ();
		SceneManager.LoadScene (scene.buildIndex + 1);
	}

}
