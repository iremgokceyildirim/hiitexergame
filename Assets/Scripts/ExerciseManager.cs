using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System;
using System.IO;
using System.Text;
using System.Threading;

namespace MSUExerGame
{
    // This class communicates with the exercise manager
    // (the computer that will send the information from the exercise machine)
	public class ExerciseManager : MonoBehaviour 
	{

        private int serverPort = 3586;
		private string serverIP = "192.168.1.5"; // specify IP
		public float Speed = 0.0f;	
		
		private TcpClient _tcpClient = new TcpClient();
		
		private Thread _networkThread;
		private StreamReader socketReader;
		
		private bool networkConnected = false;
		

		void Awake(){			
		}

		void Start () {
			_networkThread = new Thread (_networkTask);
			_networkThread.Start ();
		}
		
		protected void _networkTask() {
			string line;
			char[] delimiterChars = { ',' };
			
			try
			{
				Debug.Log("Connecting to " + IPAddress.Parse(serverIP) + " port " + serverPort);
				_tcpClient.Connect(IPAddress.Parse(serverIP), serverPort);
				socketReader = new StreamReader(_tcpClient.GetStream(), Encoding.ASCII);
				
				networkConnected = true;
				
				while(true) {
					line = socketReader.ReadLine();

					String[] receivedData = line.Split(delimiterChars);
					Speed = float.Parse(receivedData[0]);
					Debug.Log (line);
					
				}
			}
			catch(SocketException ex)
			{
				Debug.Log(ex.Message);
			}
			
			networkConnected = false;
		}
	}
}

