﻿using UnityEngine;
using System.Collections;
using MSUExerGame;

public class PlayerControl : MonoBehaviour {

	public float exSpeed;
	//public float realSpeed; //Get speed from the training bike

//	PlayerBehavior _pb;

	public ExerciseManager exManager;
	public float exSpeedConversionFactor = 4f;
	float zSpeed;
	[HideInInspector]
	public Vector3 speed;
	Rigidbody rigidBody;

	[HideInInspector]
	public bool isMoving = true;

	// Use this for initialization
	void Start () {
//		_pb = this.gameObject.GetComponent<PlayerBehavior> ();
		exManager = this.GetComponent<ExerciseManager>();
		rigidBody = this.gameObject.GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (isMoving) {
			if(exManager.Speed == 0){ //keyboard control
				if(Input.GetKey ("up"))
					exSpeed += Time.deltaTime*20;
				else if(Input.GetKey("down"))
					exSpeed -= Time.deltaTime*20;
			}
			else //exerbike control
				exSpeed = Mathf.Lerp (exSpeed, exManager.Speed, Time.deltaTime);

			zSpeed = Mathf.Lerp (zSpeed, exSpeed/exSpeedConversionFactor, Time.deltaTime);
			speed = Vector3.forward * zSpeed;
			rigidBody.velocity = speed;
		}
		else
			rigidBody.velocity = Vector3.Lerp(rigidBody.velocity, Vector3.zero, Time.deltaTime);

/*		if (isMoving & Input.GetKey (KeyCode.A)) {
			_pb.SpeedUp ();
			print ("Speed Up");
		}

		if (isMoving & moveSpeed > 0) {
			_pb.SlowDown ();
			print ("Slow Down");
		}*/
	
	}
}
