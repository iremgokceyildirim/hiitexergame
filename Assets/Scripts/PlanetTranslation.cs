﻿using UnityEngine;
using System.Collections;

public class PlanetTranslation : MonoBehaviour {

	public Transform player;
	public float directionFactor = 1;

	// Use this for initialization
	void Start () {
		transform.position = player.position;
	}
		

	// Update is called once per frame
	void Update () {
		transform.Translate(Vector3.forward * Time.deltaTime * directionFactor);
	}
}
