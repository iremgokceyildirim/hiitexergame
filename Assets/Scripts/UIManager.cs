﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public GameManager gM;
	public PlayerControl player;

	public Text speedText;
	public Text playerTargetSpeedText;
	public Text countDownTimerText;
	public AudioSource timerclicking;
	public RectTransform timerPanel;
	public Text phaseText;
	public GameObject speedWarning;
	public GameObject gameOverPanel;
	public GameObject upperInGamePanel;
	public Image bottomControlPanel;
	public GameObject targetSpeedRangeOnSpeedMeter;
	public AudioSource targetSpeedRangeAudio;
	string targetSpeedDisplayString;
	string phaseString;
	Quaternion targetRangeonSpeedMeterRotate;
	Color panelColor;


	public Text countdownTimer;
	public Animator countdownTimerAnimator;
	bool beginCountdown = true;


	void Start () {
		speedWarning.gameObject.SetActive (false);
	}

	void Update () {

		if (beginCountdown) {
			StartCoroutine (Countdown ());
		}

		if(!gM.isGameOver){
			switch(gM.phaseNum){
			case 1: phaseString = "Warm Up"; panelColor = Color.green; panelColor.a = 0.5f; upperInGamePanel.GetComponent<Image>().color = panelColor; panelColor.a = 1;bottomControlPanel.color = panelColor; break;
			case 2: phaseString = "High Intensity"; panelColor = Color.red; panelColor.a = 0.5f; upperInGamePanel.GetComponent<Image>().color = panelColor;panelColor.a = 1;bottomControlPanel.color = panelColor; break;
			case 3: phaseString = "Low Intensity"; panelColor = Color.yellow; panelColor.a = 0.5f; upperInGamePanel.GetComponent<Image>().color = panelColor;panelColor.a = 1;bottomControlPanel.color = panelColor; break;
			case 4: phaseString = "Cooling Down"; phaseText.color = Color.grey;panelColor = Color.blue; panelColor.a = 0.5f; upperInGamePanel.GetComponent<Image>().color = panelColor;panelColor.a = 1;bottomControlPanel.color = panelColor; break;
				default: break;
			}

			targetSpeedDisplayString = Mathf.RoundToInt ((gM.targetSpeed - gM.targetSpeedRange)).ToString () + " ~ " + Mathf.RoundToInt ((gM.targetSpeed + gM.targetSpeedRange)).ToString ();

			speedText.text = Mathf.RoundToInt (player.exSpeed).ToString ();
			playerTargetSpeedText.text = targetSpeedDisplayString;
			phaseText.text = phaseString;

			if (gM.countdownTimerStarts) {
				countDownTimerText.text = Mathf.RoundToInt (gM.countDownTime).ToString ();
				timerPanel.offsetMax = new Vector2((-1680/gM.countDownTimeFromFile) * (gM.countDownTimeFromFile-gM.countDownTime), 0);
				if(!(gM.phaseNum == 4 && gM.countDownTime <= 0)){
					timerclicking.enabled = true;
					if(timerclicking.isPlaying == false)
						timerclicking.Play();
				}
			} else {
				countDownTimerText.text = "0";
			}

			if(gM.targetSpeed > 90)
				targetRangeonSpeedMeterRotate= Quaternion.Euler (0, 0, -(gM.targetSpeed+20));
			else
				targetRangeonSpeedMeterRotate = Quaternion.Euler (0, 0, -(gM.targetSpeed+10));
			targetSpeedRangeOnSpeedMeter.transform.rotation = Quaternion.Lerp (targetSpeedRangeOnSpeedMeter.transform.rotation, targetRangeonSpeedMeterRotate, Time.deltaTime * 4f);

			if (player.exSpeed < gM.targetSpeed - gM.targetSpeedRange || player.exSpeed > gM.targetSpeed + gM.targetSpeedRange ) { //TODO: Not sure if we need to set "more than max" check e.g. Are they allowed to cycle more than max speed?
				//speedWarning.gameObject.SetActive (true);
				targetSpeedRangeOnSpeedMeter.GetComponent<Image>().color = Color.Lerp(Color.white, Color.red, Mathf.PingPong(Time.time*2f, 1));
				speedText.color = Color.Lerp(Color.black, new Color(0.5F, 0, 0, 1), Mathf.PingPong(Time.time*2f, 1));
				if(!targetSpeedRangeAudio.isPlaying)
					targetSpeedRangeAudio.Play();
			} else {
				targetSpeedRangeOnSpeedMeter.GetComponent<Image>().color = Color.white;
				speedText.color = Color.black;
				targetSpeedRangeAudio.Stop();
				//speedWarning.gameObject.SetActive (false);
			}
		}
		else{
			targetSpeedRangeAudio.Stop();
			StartCoroutine(showGameOverPanelAfter(5));
		}
	}

	IEnumerator showGameOverPanelAfter(float waitTime) {
		yield return new WaitForSeconds(waitTime);
		gameOverPanel.SetActive(true);
	}

	public void replay(){
		Application.LoadLevel ("Game");
	}

	IEnumerator Countdown()
	{        
		beginCountdown = false;

		countdownTimerAnimator.SetTrigger ("Disappear");
		yield return new WaitForSeconds (1);
		countdownTimer.text = "2";
		countdownTimerAnimator.SetTrigger ("Disappear");
		yield return new WaitForSeconds (1);
		countdownTimer.text = "1";
		yield return new WaitForSeconds (1);
		countdownTimerAnimator.SetTrigger ("Exit");
		countdownTimer.text = "Go!";
		yield return new WaitForSeconds (1);
		countdownTimer.gameObject.SetActive (false);
		gM.countdownOver = true;
		gM.countdownTimerStarts = true;

	}
}
