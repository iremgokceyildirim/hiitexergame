﻿using UnityEngine;
using System.Collections;

public class TakeOffTranslation : MonoBehaviour {

	public Transform targetPosition;
	public GameManager gm;
	private Vector3 velocity = Vector3.zero;

	public Transform flyAwayPosition;
	public Transform initialPosition;

	bool flyAway = false;
	// Use this for initialization
	void Start () {


	}

	// Update is called once per frame
	void Update () {
		if (gm.cameraRotate) {
			Vector3 pos = Vector3.SmoothDamp (this.transform.position, targetPosition.position, ref velocity, 1.0f);
			this.transform.position = pos;

			this.transform.LookAt (GameObject.FindGameObjectWithTag("Player").transform);
		}

		if (gm.phaseNum == 4 && gm.finalFuelValue > 0.5 ) {
			StartCoroutine (FlyAway ());

		}

	}

	IEnumerator FlyAway()
	{
		yield return new WaitForSeconds (10.0f);
		Vector3 pos = Vector3.SmoothDamp (this.transform.position, flyAwayPosition.position, ref velocity, 1.0f);
		this.transform.position = pos;

		this.transform.position += new Vector3 (-1.0f * Time.deltaTime,-1.0f*Time.deltaTime,0);

		this.transform.LookAt (GameObject.FindGameObjectWithTag("Player").transform);
	}

	IEnumerator CrashDown()
	{
		yield return new WaitForSeconds (5.0f);
		Vector3 pos = Vector3.SmoothDamp (this.transform.position, initialPosition.position, ref velocity, 0.5f);
		this.transform.position = pos;

		this.transform.position += new Vector3 (-1.0f * Time.deltaTime,-1.0f * Time.deltaTime,0);
		this.transform.RotateAround (GameObject.FindGameObjectWithTag ("Player").transform.position, Vector3.forward, 100 * Time.deltaTime);
	}
}
