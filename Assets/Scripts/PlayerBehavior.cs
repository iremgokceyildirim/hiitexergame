﻿using UnityEngine;
using System.Collections;

public class PlayerBehavior : MonoBehaviour {
	//Reference to PlayerControl to access the speed of player and movement trigger
	PlayerControl _pc;

	//Using two bools to control the status of speed up/slow down
	public bool speedUp = false;
	public bool slowDown = false;

	//Define two extreme status
	public bool overheated = false;
	public bool needToSpeedUp = false;

	public float accSpeed = 3; //Accelerate Speed in game
	public float decSpeed = 3; //Decelerate Speed in game

	public ParticleSystem takeOffFire;
	public GameManager gM;
	public GameObject explosive;
	public GameObject gameOverCamera, mainCamera;

	ParticleSystem.EmissionModule tempEmission;
	ParticleSystem.MinMaxCurve tempEmissionRate;
	bool once =false;
	// Use this for initialization
	void Start () {
		_pc = this.gameObject.GetComponent<PlayerControl> ();//Refer to PlayerControl to access the speed of player and movement trigger

	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey (KeyCode.A)) {
			SpeedUp ();
		}

		if (!gM.isGameOver) {
			tempEmission = takeOffFire.emission;
			tempEmissionRate = tempEmission.rate;

			if (gM.phaseNum == 2) {
				takeOffFire.startLifetime = 0.9f;
				tempEmissionRate.constant = 90f;

			}
			else if (gM.phaseNum == 3) {
				takeOffFire.startLifetime = 0.5f;
				tempEmissionRate.constant = 30f;
			}
			else if (gM.phaseNum == 4) {
				takeOffFire.startLifetime = 1f;
				tempEmissionRate.constant = 120f;
			}
			tempEmission.rate = tempEmissionRate;
		}
		else{
			if(!once){
				once = false;
				StopMove();
				gameOverCamera.transform.position = mainCamera.transform.position;
				gameOverCamera.transform.rotation = mainCamera.transform.rotation;
				gameOverCamera.SetActive(true);
				mainCamera.SetActive(false);
				explosive.SetActive(true);
				GetComponent<Rigidbody>().useGravity = true;
				GetComponent<Rigidbody>().AddExplosionForce(10,transform.position,3, 0, ForceMode.Impulse);
				takeOffFire.Stop();
				once = true;
			}
			else{
				gameOverCamera.transform.position = Vector3.Lerp(gameOverCamera.transform.position, new Vector3(gameOverCamera.transform.position.x, gameOverCamera.transform.position.y,mainCamera.transform.position.z + 4), Time.deltaTime*10);
				gameOverCamera.transform.LookAt(transform.position);
			}
		}
	
	}

	public void SpeedUp()//Call this method to speed up
	{
		speedUp = true;

		if (speedUp) {
			bool accelerate = true;

			if (accelerate) {
				_pc.exSpeed += accSpeed * Time.deltaTime;
				accelerate = false;
			}
		}

		speedUp = false;
	}

	public void SlowDown()//Call this method to slow down
	{
		slowDown = true;

		if (slowDown) {
			bool decelerate = true;

			if (decelerate) {
				_pc.exSpeed -= decSpeed * Time.deltaTime;
				decelerate = false;
			}
		}

		slowDown = false;
	}

	public void StopMove()
	{
		_pc.isMoving = false;
		_pc.exSpeed = 0;
	}

	public void Overheat()
	{
		//UI Warnings
	}

	public void NeedToSpeedUp()
	{
		//UI Warnings
	}
}
