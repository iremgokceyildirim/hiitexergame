﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using MSUExerGame;

public class GetMaxSpeed : MonoBehaviour {

	public float playerSpeed;

	public float maxSpeed;

	public bool riding = false;

	public float inputSpeedIncrease = 2.0f;//Comment this line when have real speed input.
	public float reduction = 0.1f;//Comment this line when have real speed input.

	public float countTime = 30.0f;

	[HideInInspector]
	public bool testing = true;

	//UI indicators
	public Text speedText;
	public Text maxSpeedText;
	public Text countDownTimerText;


	public ExerciseManager exManager;


	// Use this for initialization
	void Start () {
	
		//Get playerSpeed from the training bike;
		testing = true;

	}
	
	// Update is called once per frame
	void Update () {
	
		SpeedInput ();
		MaxSpeedDetector ();
		Countdown ();

		speedText.text = (Mathf.RoundToInt(playerSpeed) ).ToString ();
		maxSpeedText.text = "Max Speed: " + (maxSpeed).ToString() + "rpm";
		countDownTimerText.text = "Countdown Timer: " + (Mathf.RoundToInt (countTime)).ToString ();

	}

	 void SpeedInput()
	{
		if (testing) {
			//SpeedUp ();
			playerSpeed = exManager.Speed;
		} 
	}

/*	void SpeedUp()
	{
		bool speedUp = true;

		if (speedUp) {

			bool accelerated = false;

			if (!accelerated) {
				playerSpeed += (inputSpeedIncrease - reduction) * Time.deltaTime;
				accelerated = true;
			}

		}

		speedUp = false;
	}

	void SpeedDown ()
	{
		bool speedDown = true;
		if (speedDown) {
			bool decelerated = false;

			if (!decelerated) {
				playerSpeed -= reduction * Time.deltaTime;
				decelerated = true;
			}
		}

		speedDown = false;
		
	}*/

	void Countdown()
	{
		if (countTime > 0) {
			countTime -= Time.deltaTime;
		}
		if (countTime < 0) {
			testing = false;
		}
	}

	void MaxSpeedDetector()
	{
		float recordedSpeed;
		
		if (playerSpeed > maxSpeed) {
			
			recordedSpeed = playerSpeed;
			maxSpeed = recordedSpeed;
		}
	}
}
