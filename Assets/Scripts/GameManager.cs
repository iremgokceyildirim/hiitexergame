﻿using UnityEngine;
using System.Collections;


public class GameManager : MonoBehaviour {

	public PlayerControl player;

	public string phase;
	public int phaseNum;

	public GameObject cave1;
	public GameObject cave2;
	public GameObject cave3;

	public GameObject env1;
	public GameObject env2;
	public GameObject env3;
	public GameObject env4;

	public GameObject theCamera;
	public Material skyboxSpace;

	public bool cameraRotate = false;
	public Transform cameraTakeOffPosition;
	public ParticleSystem takeOffFire;
	public GameObject[] planets;
	public GameObject mothership;
	public GameObject gameEndPanel;
	public AudioSource timerclicking, warningSound;


	public float countDownTime;
	public float countDownTimeFromFile;

	public float targetSpeed;
	public float targetSpeedRange = 20;
	public float lowIntensitySpeedFactor = 20;
	int phaseCounter;

	public bool isGameOver = false;

	public bool countdownOver = false;
	public bool countdownTimerStarts = false;

	public float finalFuelValue;
	public FuelChargeManager _fcm;
	public

	// Use this for initialization
	void Start () {
		player.enabled = false;
		player.gameObject.GetComponent<PlayerBehavior> ().enabled = false;
		takeOffFire.Stop ();
//		//GetMaxSpeed ();
//		phaseNum = 1;
//		phaseCounter = 0;
//		countDownTime = PlayerPrefs.GetFloat("WarmUpDuration");

		env1.SetActive (false);
		env2.SetActive (false);
		env3.SetActive (false);
		env4.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {


		if (countdownOver) {
			player.enabled = true;
			player.gameObject.GetComponent<PlayerBehavior> ().enabled = true;
			takeOffFire.Play ();
			phaseNum = 1;
			phaseCounter = 0;
			countDownTime = PlayerPrefs.GetFloat("WarmUpDuration");
			countDownTimeFromFile = PlayerPrefs.GetFloat("WarmUpDuration");
			countdownOver = false;
		}

		if (!isGameOver) {

			if (phaseNum == 2) {
				CameraRotation ();
			}

			countDownTime -= Time.deltaTime;
			//Use One Countdown Timer and execute stages under this countdown timer.

			if (phaseNum == 1) {
				WarmUp ();
			}
			if (phaseNum == 2) {
				if (player.gameObject.transform.position.z - cave2.transform.position.z > 11.045) {

					env1.SetActive (true);
					env2.SetActive (true);
					env3.SetActive (true);
					env4.SetActive (true);
				}
				HighIntensityPhase ();
			}
			if (phaseNum == 3) {
				LowIntensityPhase ();
			}

			if (phaseNum == 4) {

				CoolDown ();
			}
		}
		else{
			timerclicking.enabled = false;
		}
	}

/*	void GetMaxSpeed()
	{
		if (PlayerPrefs.HasKey ("PlayerMaxSpeed")) {
			playersMaxSpeed = PlayerPrefs.GetFloat ("PlayerMaxSpeed");
		}
	}*/

	void WarmUp()
	{
		if (countDownTime <= 0) {
			phaseNum = 2;
			countDownTime = PlayerPrefs.GetFloat("HIDuration");
			countDownTimeFromFile = PlayerPrefs.GetFloat("HIDuration");
			cave1.GetComponent<CaveRecycle> ().enabled = false;
			cave2.GetComponent<CaveRecycle> ().enabled = false;
			cave3.GetComponent<CaveRecycle> ().enabled = false;
		
			//cave1.SetActive(false);
			//cave2.SetActive(false);
			//cave3.SetActive(false);

		} else {
			targetSpeed = PlayerPrefs.GetInt("WarmUpSpeed");
		}
	}

	void HighIntensityPhase()
	{
				//countDownTime -= Time.deltaTime;
		//print ("countDownTime: " + countDownTime.ToString ());
		//print ("HighIntensity begins");
		if (countDownTime <= 0) {
			countDownTime = PlayerPrefs.GetFloat("LIDuration");
			countDownTimeFromFile = PlayerPrefs.GetFloat("LIDuration");
			phaseCounter++;
			phaseNum = 3;
			print ("HIP ended");

			cave1.SetActive(false);
			cave2.SetActive(false);
			cave3.SetActive(false);
		} else {
			targetSpeed = PlayerPrefs.GetInt("HISpeed");
		}	
	}

	void LowIntensityPhase()
	{
		if (countDownTime <= 0 && phaseCounter<4) {
			countDownTime = PlayerPrefs.GetFloat("HIDuration");
			countDownTimeFromFile = PlayerPrefs.GetFloat("HIDuration");
			phaseCounter++;
			phaseNum = 2;

		} else if (countDownTime <= 0 && phaseCounter >= 4 ) {
			countDownTime = PlayerPrefs.GetFloat("CoolDownDuration");
			countDownTimeFromFile = PlayerPrefs.GetFloat("CoolDownDuration");
			phaseNum = 4;

		} else {
			targetSpeed = PlayerPrefs.GetInt("LISpeed");
		}
	}

	void CoolDown()
	{
		env1.GetComponent<BackgroundRecycle> ().enabled = false;
		env2.GetComponent<BackgroundRecycle> ().enabled = false;
		env3.GetComponent<BackgroundRecycle> ().enabled = false;
		env4.GetComponent<BackgroundRecycle> ().enabled = false;
		foreach (GameObject planet in planets) {
			planet.SetActive (true);
		}
		mothership.SetActive(true);

		cameraRotate = true;
		theCamera.GetComponent<Skybox> ().material = skyboxSpace;
		takeOffFire.Play ();


		if (countDownTime <= 0) {
			Time.timeScale = 0;
			gameEndPanel.SetActive(true);
			timerclicking.enabled = false;
			warningSound.enabled = false;

		} else {
			targetSpeed = PlayerPrefs.GetInt ("CoolDownSpeed");
		}
	}

	//camera
	void CameraRotation()
	{
		if (theCamera.transform.rotation.eulerAngles.y < 180) {
			theCamera.transform.RotateAround (player.transform.position, Vector3.up, 100 * Time.deltaTime);
		}
	}
}
