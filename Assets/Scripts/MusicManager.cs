﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

	public AudioSource gameAudio;
	public AudioSource playerAudio;
	public AudioClip warmingUpBgMusic, highIntensityBgMusic, lowIntensityBgMusic, coolingDownBgMusic, gameOverBgMusic, startCountdownMusic;
	public AudioClip playerSlow, playerMed, playerFast,playerVeryFast, playerExplode;
	public GameManager gM;
	bool once = false;


	bool countdownSoundPlayed = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!gM.countdownOver && !gM.countdownTimerStarts && !countdownSoundPlayed) {
			gameAudio.PlayOneShot(startCountdownMusic);
			playerAudio.clip = null;
			countdownSoundPlayed = true;
		}

		else{
			if(!gM.isGameOver){
				if (gM.phaseNum == 1){
					gameAudio.clip = warmingUpBgMusic;	
					playerAudio.clip = playerMed;	
				}
				if (gM.phaseNum == 2){
					gameAudio.clip = highIntensityBgMusic;
					playerAudio.clip = playerFast;	
				}
				else if (gM.phaseNum == 3){
					gameAudio.clip = lowIntensityBgMusic;
					playerAudio.clip = playerSlow;	
				}
				else if (gM.phaseNum == 4){
					gameAudio.clip = coolingDownBgMusic;
					playerAudio.clip = playerVeryFast;	
				}

				if (!gameAudio.isPlaying) {
					gameAudio.Play();
				}
				if (!playerAudio.isPlaying) {
					playerAudio.Play();
				}
			}
			else{
				if(!once){
					gameAudio.clip = gameOverBgMusic;
					gameAudio.loop = false; // TODO: not working
					playerAudio.clip = playerExplode;
					playerAudio.loop = false;
					gameAudio.Play();
					playerAudio.Play();
					once = true;
				}
			}
		}
	}
}
